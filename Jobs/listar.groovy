node('master') {

    println "${params.Project}"
    def project = params.Project
    machine_user = "master"
    switch (project){
        case "Erudi":
        machine_ip = "35.202.225.184"
        sh '''
            ssh '''+machine_user+'''@'''+machine_ip+''' sudo docker images
 	    '''
        break
        case "Med360":
        sh '''
            /var/lib/jenkins/google-cloud-sdk/bin/kubectl get nodes
 	    '''
        break
        case "Vantaz":
        machine_ip = "35.193.69.43"
        sh '''
            ssh '''+machine_user+'''@'''+machine_ip+''' sudo docker images
 	    '''
        break
    }
}

return this;

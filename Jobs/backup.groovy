node('master') {

    println "${params.Project}"
    project = params.Project
    
    switch (project){
        case "Startups":
        machine_ip = "35.232.204.83"
        machine_user = "master"
        container_name = "wordpress-docker_backend_1"
        bucket_folder = "startups-backup"
        time_backup = "7"
        break

        case "DGAF":
        machine_ip = "35.192.102.107"
        machine_user = "master"
        container_name = "wordpress-docker_backend_1"
        bucket_folder = "dgaf-backup"
        time_backup = "7"
        break
        
        case "Vantaz":
        name = "vantaz"
        machine_ip = "35.193.69.43"
        machine_user = "master"
        container_name = "wordpress-docker_backend_1"
        bucket_folder = "vantaz-backup"
        time_backup = "14"
        break
    }

    stage ('delete_old'){

        sh '''    

        ssh '''+machine_user+'''@'''+machine_ip+''' 'find '''+bucket_folder+''' -mtime +'''+time_backup+''' -type f -exec rm -fv {} \\;'
        sleep 3

        ssh '''+machine_user+'''@'''+machine_ip+''' 'sudo find copia-de-seguridad -name '*'''+project+'''*' -type d -exec rm -rf {} \\; || true'

            
        '''     

        println "=========================== Hecho ==========================="
    }

    stage ('app_backup'){
        
        date_time = sh (script: ' ssh '+machine_user+'@'+machine_ip+' date "+%F_%H.%M.%S" ', returnStdout: true).trim()
       
        sh '''
            ssh '''+machine_user+'''@'''+machine_ip+''' mkdir /home/master/copia-de-seguridad/'''+project+'''-'''+date_time+'''
            sleep 2

            ssh '''+machine_user+'''@'''+machine_ip+''' sudo docker cp '''+container_name+''':/var/www/html /home/master/copia-de-seguridad/'''+project+'''-'''+date_time+'''

            sleep 5
 	    '''
        println "=========================== Hecho ==========================="


    }
    if(project=="Vantaz"){
    stage ('db_backup_vantaz'){
      withCredentials([
        string(credentialsId: 'vantaz-bd', variable: 'vantaz_key'),
        string(credentialsId: 'vantaz-user', variable: 'vantaz_user'),
        string(credentialsId: 'vantaz-name', variable: 'vantaz_name'),
        string(credentialsId: 'vantaz-db-host', variable: 'vantaz_db_host')
    
    ]){
        sh '''

        ssh '''+machine_user+'''@'''+machine_ip+''' 'mysqldump --host='''+vantaz_db_host+''' --user='''+vantaz_user+''' --password='''+vantaz_key+''' --column-statistics=0 --set-gtid-purged=OFF '''+vantaz_name+'''  > /home/master/copia-de-seguridad/'''+project+'''-'''+date_time+'''/db_backup.sql'

        sleep 5

      '''
    }

        println "=========================== Hecho ==========================="
    }
    }

    if(project=="DGAF"){
    stage ('db_backup_dgaf'){
      withCredentials([
        string(credentialsId: 'dgaf-bd', variable: 'dgaf_key'),
        string(credentialsId: 'dgaf-user', variable: 'dgaf_user'),
        string(credentialsId: 'dgaf-name', variable: 'dgaf_name'),
        string(credentialsId: 'dgaf-db-host', variable: 'dgaf_db_host')
    
    ]){
        sh '''

        ssh '''+machine_user+'''@'''+machine_ip+''' 'mysqldump --host='''+dgaf_db_host+''' --user='''+dgaf_user+''' --password='''+dgaf_key+''' --column-statistics=0 --set-gtid-purged=OFF '''+dgaf_name+'''  > /home/master/copia-de-seguridad/'''+project+'''-'''+date_time+'''/db_backup.sql'

        sleep 5

      '''
    }

        println "=========================== Hecho ==========================="
    }
    }

    if(project=="Startups"){
    stage ('db_backup_startups'){
      withCredentials([
        string(credentialsId: 'startups-bd', variable: 'startups_key'),
        string(credentialsId: 'startups-user', variable: 'startups_user'),
        string(credentialsId: 'startups-name', variable: 'startups_name'),
        string(credentialsId: 'startups-db-host', variable: 'startups_db_host')
    
    ]){
        sh '''

        ssh '''+machine_user+'''@'''+machine_ip+''' 'mysqldump --host='''+startups_db_host+''' --user='''+startups_user+''' --password='''+startups_key+''' --column-statistics=0 --set-gtid-purged=OFF '''+startups_name+'''  > /home/master/copia-de-seguridad/'''+project+'''-'''+date_time+'''/db_backup.sql'

        sleep 5

      '''
    }

        println "=========================== Hecho ==========================="
    }
    }
    

    stage ('tar_up'){

        sh '''    

        ssh '''+machine_user+'''@'''+machine_ip+''' sudo tar -czvf /home/master/copia-de-seguridad/'''+project+'''-'''+date_time+'''.tar.gz /home/master/copia-de-seguridad/'''+project+'''-'''+date_time+'''

            sleep 30

            ssh '''+machine_user+'''@'''+machine_ip+''' mv /home/master/copia-de-seguridad/'''+project+'''-'''+date_time+'''.tar.gz /home/master/'''+bucket_folder+'''
            
        '''     

        println "=========================== Hecho ==========================="
    }

    

    
}

return this;

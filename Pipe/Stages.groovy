def call() {
  //Guardamos dentro de la variable stages lo ingresado por parametros, y se separa por punto y coma en caso de ejecutar más de un stage al mismo tiempo
 def stages = "${Stage}".split(';')

  //Defino una variable de entorno que obtiene el nombre del repositorio desde la url de git
 env.repo=env.GIT_URL.replaceFirst(/^.*\/([^\/]+?).git$/, '$1')
 
 println "------- Obteniendo Jenkins pipeline ---------"
 //Se abre la shell que clona el repositorio con los metodos a utilizar e invocar
  sh """
    cd ..
    mkdir option_pipeline &
    sleep 2

    cd option_pipeline
    git clone https://gitlab.com/matsiv/jenkins.git || true

    sleep 2
    cd jenkins

    git pull

    cp -r Pipe ${WORKSPACE}
    """
 //Cargando métodos para todos los stages   
 //Se externalizan los métodos para orden y fines de buenas prácticas.
 def metodos = load('Pipe/Metodos.groovy')

//Se define el stage de build, custome por cada tipo de proyecto
  stage('Build'){
//Se agregan validaciones en todos los stages para parámetros de entrada por jenkins, vacio = se ejecuta el stage, nombre del stage= se ejecuta el stage.    
    if(stages.contains('Build') || "${Stage}" ==''){
      if (env.type=='js') {
        println '====== No aplica BUILD o no se asignó variable ====='
      }else{
        println '====== No aplica BUILD o no se asignó variable ====='
      }
    }
  }

//Se define el stage de test, custome por cada tipo de proyecto
  stage('PruebasUnitarias'){
    if(stages.contains('PruebasUnitarias') || "${Stage}" ==''){
      if (env.type=='js') {
        println '====== Pruebas exitosas ====='
      }else{
        println '====== No tiene pruebas unitarias ====='
      }
    }
  } 

//Se define el stage de empaquetado docker, y se pushea a dockerhub.
  stage('Package'){
    if(stages.contains('Package') || "${Stage}" ==''){
      metodos.packageDocker()
      metodos.pushDocker()
    }
  }  

//Se Realiza el deploy en kubernetes
  stage('Deploy'){
    if(stages.contains('Deploy') || "${Stage}" ==''){
      metodos.deployKubernetes()
    }
  } 

//Se Realiza el deploy en kubernetes
  stage('SmokeTest'){
    if(stages.contains('SmokeTest') || "${Stage}" ==''){
      println '====== Pruebas de humo Exitosas ====='
    }
  } 

}
return this;

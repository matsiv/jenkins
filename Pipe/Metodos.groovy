//Se contruye la imagen docker y se tagea para ser utilizada.
//Se utiliza la variable WORKSPACE(por defecto el path de la ejecución dentro del ambiente).
//Se utiliza la variable repo que toma el nombre del repositorio y la variable BRANCH_NAME que toma el nombre de la rama.
//Todo esto es necesario para tagear la imagen y referenciarla luego, para que se distingan de otros repositorio en la automatización.
//Se aplican sleep en los scripts solo para evitar posibles errores de lentitud en el ambiente.
def packageDocker(){
    
      sh """
      cd ${WORKSPACE}
      sudo docker build -t sieven/'${repo}:${BRANCH_NAME}' .

    """
}

//Aplicamos la configuración para levantar pod, se toma directo del repositorio actualizado y se aplica.
def deployKubernetes(){
  sh """
      cd ${WORKSPACE}
      git checkout ${BRANCH_NAME}

      sleep 2

      git pull

      sleep 2

      kubectl apply -f Kubernetes.yaml 

    """  
}

//Me logeo a una cuenta de dockerhub desde donde hago el pull a minikube
//Hago push de la imagen docker 
//Se entrega el acceso en duro (Solo para fines prácticos)
def pushDocker(){
  sh """  
      sudo docker login -u="sieven" -p="jenkins.123"

      sleep 5
      sudo docker push sieven/'${REPO}:${BRANCH_NAME}'

    """
}

return this;